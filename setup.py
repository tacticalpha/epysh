
from setuptools import setup, find_packages

setup(
    name = 'embedded-python-shell',

    version     = '1.0.0',
    description = 'A package to embed a unix shell style input into a python script',
    url         = 'https://gitlab.com/tacticalpha/epysh',
    author      = 'Ryn',

    packages = find_packages(),
)
