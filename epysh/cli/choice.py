
from epysh import Styles

def yesno(prompt=None, *, default=True, hint_text=None):
    """
    :param prompt: Can be a str or falsey for no prompt
    :type prompt: str

    :param default: Default result if input is blank or `None` to require input.
    :type default: bool, None

    :param hint_text: Hint text to replace normal 'y/n' hint
    :type hint_text: str, None
    """
    prompt_str = ''
    if prompt:
        prompt_str += f'{prompt} '

    if hint_text:
        prompt_str += f'{hint_text}'
    elif default == True:
        prompt_str += '(y)/n'
    elif default == False:
        prompt_str += 'y/(n)'
    else:
        prompt_str += 'y/n'

    while True:
        print(prompt_str, end='')
        selection = input(' ').lower().strip()

        if selection == '' and default is not None:
            break

        elif not selection and default is None:
            print('A choice is required!')
            continue

        elif selection not in yesno.acceptable_inputs:
            print('Acceptable inputs are '
                  + (', '.join(yesno.acceptable_inputs[0:-1]))
                  + ', and '
                  + yesno.acceptable_inputs[-1]
            )
            continue

        break

    if selection == '':
        return bool(default)

    return selection in yesno.truthy

yesno.truthy = ('yes', 'y')
yesno.acceptable_inputs = (*yesno.truthy, 'no', 'n')

def list(prompt, *choices, default=None, required=False, allow_str=False,
         return_type=0, zero_index=False, styles=None):
    """
    :param prompt: Can be a str or falsey for no prompt
    :type prompt: str, None

    :param choices: Choices to be presented to the user. If len is only 1 and
                    choices[0] is of type `list`, that'll be used as the choices
    :type choices: list

    :param default: Default selection if `required` kwarg is False and input is
                    empty. If set to none, `None` will be returned on no input,
                    defaults to `None`
    :type default: bool, None

    :param required: Requires a user choice if `True`, `default` is ignored if
                     this is set, defaults to `False`
    :type required: bool

    :param allow_str: Allows a free input if this is true, if `return_type`
                      provides the choice index, it will be `False` instead of
                      an index, defaults to `False`
    :type allow_str: bool

    :param return_type: Can either be `0` for just index, `1` for just selected
                        value, or `2` for a tuple of (index, value), defaults to
                        `0`
    :type return_type: int

    :param zero_index: Presents the list as starting zero instead of one to
                       user. Doesn't affect actual return value, defaults to
                       `False`
    :type zero_index: bool

    :param styles: TBD
    :type styles: :class:`epysh.Styles`
    """

    if not styles:
        styles = list.default_style
    else:
        styles = Styles.apply(None, list.default_style, styles)

    prompt_color = styles.get('prompt_color')
    if prompt:
        print(f'{prompt_color}{prompt}')

    index_color = styles.get('index_color')
    value_color = styles.get('value_color')
    for i, value in enumerate(choices, 0 if zero_index else 1):
        print(f'{index_color}[{i}] {value_color}{value}')

    if allow_str:
        print(prompt_color + '... or type your own.')

    choices_n = len(choices)
    selection_str = None
    selection_int = None
    while True:
        print(prompt_color + 'Choice' + (f' ({default})' if default else ''), end='')
        selection_str = input(' > ')
        selection_int = None
        try:
            selection_int = int(selection_str) - (0 if zero_index else 1)
        except ValueError:
            pass

        if required and selection_str == '':
            print('Choice required!')
            continue

        elif selection_str == '':
            if default is None:
                return None
            elif type(default) == int:
                selection_int = default
                selection_str = choices[default]
            else:
                selection_int = False
                selection_str = default
            break

        elif ((selection_int is None and not allow_str)
             or (selection_int is not None and (selection_int < 0 or selection_int > choices_n - 1))):
            if zero_index:
                range_str = f'(0 -> {choices_n - 1})'
            else:
                range_str = f'(1 -> {choices_n})'

            print(f'Out of range! {range_str}')
            continue

        elif selection_int is not None:
            selection_str = choices[selection_int]
            break

        elif allow_str:
            selection_int = False
            break

        else:
            print('uhhh ok so this is awkward', {
                'selection_str': selection_str,
                'selection_int': selection_int,
                'choices_n': choices_n
            })

    if return_type == 0:
        return selection_int
    elif return_type == 1:
        return selection_str
    elif return_type == 2:
        return (selection_int, selection_str)

list.default_style = Styles({
    'prompt_color<color>': 'normal',
    'index_color<color>': 'bright_red',
    'value_color<color>': 'bright_blue',
})
