
import os


# def get_size():
#     lines, columns = os.popen('stty size', 'r').read().split()
#     return Size(columns=columns, lines=lines)
#
# def get_lines():
#     return int(os.popen('stty size', 'r').read().split()[0])
#
# def get_columns():
#     return int(os.popen('stty size', 'r').read().split()[1])

def get_size():
    """
    This is just an alias for ``os.get_terminal_size``

    :rtype: :class:`os.terminal_size`
    """
    return os.get_terminal_size()

def get_lines():
    """
    :return: The ``lines`` attribute of :func:`epysh.esc.terminal.get_size`
    :rtype: int
    """
    return get_size().lines

def get_columns():
    """
    :return: The ``columns`` attribute of :func:`epysh.esc.terminal.get_size`
    :rtype: int
    """
    return get_size().columns

use_alternate_buffer = '\033[?1049h\033[22;0;0t'
use_normal_buffer = '\033[?1049l\033[23;0;0t'

clear_screen = '\033[2J'
clear_line = '\033[0K'
