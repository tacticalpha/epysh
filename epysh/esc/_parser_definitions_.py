
from epysh.esc.parser import define_char
from epysh.esc.parser import define_sequence
from epysh.esc.parser import define_mouse_event

define_char('backspace', '\x7f')

define_sequence('up',        '[A')
define_sequence('down',      '[B')
define_sequence('right',     '[C')
define_sequence('left',      '[D')
define_sequence('insert',    '[2~')
define_sequence('delete',    '[3~')
define_sequence('home',      '[H', 'OH')
define_sequence('end',       '[F', 'OF')
define_sequence('page_up',   '[5~')
define_sequence('page_down', '[6~')


define_sequence('f1',  'QP', 'OP')
define_sequence('f2',  'QQ', 'OQ')
define_sequence('f3',  'QR', 'OR')
define_sequence('f4',  'QS', 'OS')
define_sequence('f5',  '[15~')
define_sequence('f6',  '[17~')
define_sequence('f7',  '[18~')
define_sequence('f8',  '[19~')
define_sequence('f9',  '[20~')
define_sequence('f10', '[21~')
define_sequence('f11', '[23~')
define_sequence('f12', '[24~')


define_mouse_event('left_click',   0)
define_mouse_event('middle_click', 1)
define_mouse_event('right_click',  2)
define_mouse_event('scroll_up',    64)
define_mouse_event('scroll_down',  65)
define_mouse_event('mouse_move',   35)
