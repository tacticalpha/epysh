import re

_this_ = vars()

reset = '\033[0m'
normal = '\033(B\033[m'

color_names = [
    'black',
    'red',
    'green',
    'yellow',
    'blue',
    'magenta',
    'cyan',
    'white',
]

color_names = color_names + [ f'bright_{name}' for name in color_names ]

for i, name in enumerate(color_names):
    if name.startswith('bright_'):
        _this_[f'{name}'] = f'\033[{90 + i % 8}m'
        _this_[f'bg_{name}'] = f'\033[{100 + i % 8}m'
    else:
        _this_[name]         = f'\033[{30 + i}m'
        _this_[f'bg_{name}'] = f'\033[{40 + i}m'


hex_regexp = re.compile('^#?([0-9a-f]{2})([0-9a-f]{2})([0-9a-f]{2})$')
def get(color, *, bg=False):
    """Returns an ansi escape code for the requested color

    :param color: Can be `normal`, `reset`, the name of any of the color
        constants, a number between 0 and 255 inclusive (`<https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797#256-colors>`_), or a hex color code
        (``#rrggbb``, case insensitive)
    :type color: str

    :param bg: Return an escape code to change the background color instead of
        foreground color, defaults to `False`
    :type bg: bool, optional

    :returns: An ansi escape code
    :rtype: str
    """
    fg = not bg

    if type(color) != str:
        color = str(color)
    color = color.lower().strip()

    color_type = None
    if color == 'normal':
        return normal
    if color in color_names:
        color_type = 0
    elif color.isnumeric() and 0 <= int(color) <= 255:
        color_type = 1
    else:
        hex_match = hex_regexp.match(color)
        if hex_match:
            color_type = 2

    if color_type is None:
        raise KeyError('unknown color type')

    if color_type == 0:
        return _this_[color if fg else f'bg_{color}']

    if color_type == 1:
        return f'\033[{38 if fg else 48};5;{color}m'

    if color_type == 2:
        r, g, b = hex_match.groups()
        return f'\033[{38 if fg else 48};2;{r};{g};{b}m'
