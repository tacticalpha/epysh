
import time
import sys
import re

from epysh.classes import Coords

import epysh.stdin_reader as reader


home = '\033[H'

invisible = '\033[?25l'
visible   = '\033[?25h'

echo_cursor_pos = '\033[6n'

save_pos_term = '\033 7'
restore_pos_term = '\033 8'

def goto(coords=None, *, column=None, line=None):
    '''Returns an ansi escape code to move the terminal cursor to the specified
    column and line.

    :param coords: Initially sets column and line to match this argument,
        defaults to None
    :type coords: :class:`epysh.classes.Coords`, optional

    :param column: Column, or x, to go to. Overwrites column from `coords`
        argument, if provided, defaults to None
    :type column: int, optional

    :param line: Line, or y, to go to. Overwrites line from `coords`
        argument, if provided, defaults to None
    :type line: int, optional

    :returns: An ansi escape code. If `coords`, `column`, or `line` all aren't
        provided, returns home (1, 1)
    :rtype: str
    '''

    if type(coords) == Coords:
        if line is None:
            line = coords.line
        if column is None:
            column = coords.column

    if column is not None and line is None:
        return f'\033[{column}G'
    if line:
        # You can't go to a line without also specifying a column :(
        if column is None:
            column = get_pos().column

        return f'\033[{line};{column}H\033[{line};{column}f'
    else:
        return home


get_pos_regexp = re.compile('\x1b\[([0-9]+);([0-9]+)R')
def get_pos(*, dont_echo=False):
    """Gets current cursor position from the terminal.

    *Warning*:
    This function is slow, it has to start an stdin_reader worker then query the
    terminal before waiting for a response, use sparingly.
    """
    if dont_echo:
        return echo_cursor_pos

    try:
        started_reader = False
        if not reader.is_alive():
            started_reader = True
            reader.start()
        sys.stdout.write(echo_cursor_pos)
        sys.stdout.flush()
        response = reader.pull()
    finally:
        if started_reader:
            reader.stop(wait=False)

    response = ''.join(response)
    match = get_pos_regexp.match(response)
    line, column = match.groups()
    coords = Coords(line=int(line), column=int(column))

    if started_reader and reader.is_alive():
        reader.stop(wait=True)

    return coords


saved_positions = {}
def save_pos(k='unnamed'):
    saved_positions[k] = get_pos()
    return saved_positions[k]

def restore_pos(k='unnamed'):
    pos = saved_positions[k]
    return goto(line=pos.line, column=pos.column)

def up(n):
    '''
    :returns: An ansi escape code to move the cursor up `n` lines
    :rtype: str
    '''
    return f'\033[{n}A'

def down(n):
    '''
    :returns: An ansi escape code to move the cursor down `n` lines
    :rtype: str
    '''
    return f'\033[{n}B'

def right(n):
    '''
    :returns: An ansi escape code to move the cursor right `n` columns
    :rtype: str
    '''
    return f'\033[{n}C'

def left(n):
    '''
    :returns: An ansi escape code to move the cursor left `n` columns
    :rtype: str
    '''
    return f'\033[{n}D'

def clear_line_after():
    return f'\033[0K'
