
import select
import sys
import re

class ParsedInput(object):
    """
    The return struct for :func:`epysh.esc.parser.parse`
    """
    __slots__ = ['name', 'type', 'data']
    def __init__(self, name, *, type='key', data=None):
        self.name = name

        if type not in ParsedInput.__valid_types__:
            raise ArgumentError(f'invalid code type "{type}"')
        self.type = type

        self.data = data

    def get(self, k):
        if not self.data:
            return None
        try:
            return self.data[k]
        except KeyError:
            return None

    def __str__(self):
        return self.__repr__()
    def __repr__(self):
        return (
            f'<ParsedInput type=\'{self.type}\' name={repr(self.name)} '
            f'data={repr(self.data)}>'
        )
    @property
    def special(self):
        return self.type == 'special'
    @property
    def unknown(self):
        return self.type == 'unknown'

ParsedInput.__valid_types__ = ['key', 'special', 'MouseEvent', 'unknown']

class EscapeSequence(object):
    __slots__ = ['name', 'sequences']
    def __init__(self, name, sequences):
        self.name = name
        self.sequences = sequences

    def __str__(self):
        return f'<EscapeSequence name={self.name} sequences={repr(self.sequences)}'


class MouseEvent(object):
    __slots__ = ['column', 'line', 'state']
    def __init__(self, column, line, state=None):
        self.column = column
        self.line = line
        self.state = state

    def __str__(self):
        return f'<MouseEvent column={self.column} line={self.line} state={self.state}'

# https://donsnotes.com/tech/charsets/ascii.html
escape_chars = []
def define_char(name, char):
    """
    Defines an escaped character e.g. ``\\x7f`` is backspace

    See :meth:`esc.parser.escape_char`

    :param name: Unique name to identify the char
    :type name: str

    :param char: The single character to match
    :type char: str
    """
    escape_chars.append((name, char))


escape_sequences = []
def define_sequence(name, *sequences, esc='\x1b'):
    """
    Defines an escape sequence. Some have different codes in different
    terminals so multiple can be assigned to any sequence name.

    See :meth:`esc.parser.escape_sequences`

    :param name: Unique name to identify the sequence
    :type name: str

    :param \*sequences: All strings that this sequence will match
    :type \*sequences: str

    :param esc: First character, defaults to ``\\x1b``
    :type esc: str
    """
    # sequence = esc + sequence
    sequences = [ esc + seq for seq in sequences ]
    struct = EscapeSequence(name, sequences)
    # if esc not in escape_sequences:
    #     escape_sequences[esc] = []

    escape_sequences.append(struct)


mouse_events = {}
def define_mouse_event(name, id):
    """
    Defines a mouse event by numeric ID e.g. ``0`` is left click.

    See :meth:`esc.parser.mouse_events`

    :param name: Unique name to identify the event
    :type name: str

    :param char: The id of the event to match
    :type char: str
    """
    mouse_events[int(id)] = name

mouse_event_regexp = re.compile('^\x1b\[<([0-9]+);([0-9]+);([0-9]+)(.)')
# ansi_regexp = re.compile(r'\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])')
def parse(data):
    """
    Parses passed a parsed string or list of strings objects. This usually won't
    need to be called directly, :func:`epysh.stdin_reader.pull_and_parse` does
    it automatically.

    :param data: Data to be parsed
    :type data: list[str], str

    :returns: A list of :class:`epysh.esc.parser.ParsedInput`
    :rtype: list
    """
    keys = []
    if type(data) == list:
        data = ''.join(data)
    # print('data:', repr(data))

    def pop_match(match=None):
        nonlocal data
        # read_index += len(match)
        if match is None:
            data = ''
        elif type(match) == int:
            data = data[match:]
        else:
            data = data[len(match):]
            # print('pop:', repr(match), f'({len(match)})', ', remaining:', repr(data))


    def match(v, *args, **kwargs):
        nonlocal keys

        res = False
        # if type(v) == int and v == 1:
        #     if data:
        #         res = data[0]
        #         pop_match(1)

        if type(v) == str:
            res = data.startswith(v)
            if res:
                # data = data.lstrip(v)
                pop_match(v)

        elif type(v) == EscapeSequence:
            # res = v.match(data)
            for seq in v.sequences:
                res = data.startswith(seq)
                if res:
                    # data = data.lstrip(seq)
                    pop_match(seq)
                    break

        if res:
            keys.append(ParsedInput(*args, **kwargs))

        return res

    def loop(data):
        nonlocal keys
        for name, char in escape_chars:
            if match(char, name, type='special'):
                return

        if data[0] == '\x1b':
            if len(data) == 1:
                keys.append(ParsedInput('escape', type='special'))
                pop_match(data)

            if data.startswith('\x1b[<'):
                res = mouse_event_regexp.match(data)
                if res:
                    # data = data.lstrip(res.group())
                    pop_match(res.group())

                    id = int(res.group(1))
                    ev_data = MouseEvent(res.group(2), res.group(3))

                    if 0 <= id <= 2:
                        ev_data.state = 'up' if res.group(4) == 'm' else 'down'

                    keys.append(ParsedInput(
                        mouse_events[id] if id in mouse_events else id,
                        type='MouseEvent',
                        data=ev_data
                    ))

                    return

            for definition in escape_sequences:
                # print(definition)
                if match(definition, definition.name, type='special'):
                    return

            keys.append(ParsedInput(repr(data[1:]), type='unknown'))
            pop_match()
            return

        keys.append(ParsedInput(data[0]))
        pop_match(1)

    while data:
        # print('-> looping')
        loop(data)

    return keys
