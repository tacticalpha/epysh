
_this_ = vars()

reset = '\033[0m'

styles = (
    'bold',
    'dim',
    'italic',
    'underline',
    'blinking',
    None,
    ('invert', 'inverted'),
    'invisible',
    'strikethrough'
)

for i, names in enumerate(styles, start=1):
    if names is None:
        continue
    if type(names) == str:
        names = (names,)

    for name in names:
        _this_[name]          = f'\033[{i}m'
        _this_[f'{name}_off'] = f'\033[2{1 if i <= 2 else i}m'
        _this_[f'un{name}']   = _this_[f'{name}_off']
