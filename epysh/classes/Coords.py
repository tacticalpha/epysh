
class Coords(object):
    __slots__ = ['column', 'line']
    def __init__(self, *, column, line):
        self.column = column
        self.line   = line

    def __iter__(self):
        return iter((self.column, self.line))

    def __repr__(self):
        return f'<coords column={self.column} line={self.line}>'

    @staticmethod
    def consolidate(object=None, column=None, line=None):
        if object is not None and type(object) == Coords:
            if line is None and column is None:
                return object

            if line is None:
                line = coords.line
            if column is None:
                column = coords.column

        return Coords(column=column, line=line)

    def set(self, column, line):
        self.column = column
        self.line   = line
