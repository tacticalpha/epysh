
import threading
import termios
import select
import signal
import time
import sys
import tty
import os

from epysh.classes import Namespace
from epysh.shell import Lexer

import epysh.stdin_reader as reader
import epysh.esc as esc

def clamp(x, min=None, max=None):
    if max is not None and x >= max:
        return max
    if min is not None and x <= min:
        return min
    return x

def default_prompt(ctx=None):
    return ' > '

default_lexer = Lexer()
default_lexer.add('terminator', ';', style=esc.color.bright_red)
default_lexer.add('whitespace', '\s+', unimportant=True)
default_lexer.add(
    'flag',
    '(-[a-zA-Z0-9]+)|(--\w+)',
    style=esc.color.bright_black
)
default_lexer.add(
    'flag_value',
    '''=?(?:(\w+)|('.+')|(".+"))''',
    condition=lambda *, ctx: ctx.tokens and ctx.tokens[-1].type == 'flag',
    style=esc.color.bright_black
)
default_lexer.add(
    'quote',
    '''(".+")|('.+')''',
    style=esc.color.yellow
)
default_lexer.add(
    'argument',
    '[^\s;]+',
    condition=lambda *, ctx: ctx.tokens and ctx.tokens[-1].type in ('command', 'argument'),
    style=esc.color.cyan
)
default_lexer.add(
    'command',
    '[^\s;]+',
    style=esc.color.blue
)

class Shell:
    """
    :param prompt: A string or a function that returns a string to be used as
        the shell prompt, defaults to ``' > '``
    :type prompt: str, function, optional

    :param lexer: The :class:`epysh.shell.Lexer` for determining the syntax highlighting of the
        shell, defaults to :attr:`epysh.Shell.default_lexer`
    :type lexer: :class:`epysh.shell.Lexer`, optional

    """
    def __init__(self, *, prompt=' > ', lexer=default_lexer):
        self.prompt = prompt
        self.lexer = lexer

        # self.autocomplete = Namespace(
        #     commands={}
        # )

        self.state = Namespace(
            stack         = [],
            index         = 0,
            r_index       = 0,
            prompt_width  = 0,
            history       = [[]],
            history_index = 0,

            h_overflow = False,
            h_scroll   = 0,

            pos_line   = None,
            pos_column = None,
            max_width  = None,
            max_column = None,
        )

    def input(self, prompt=None, *, pos_line=None, pos_column=None, max_width=None):
        """
        A drop-in replacement for python's ``input``. This uses
        the :mod:`epysh.stdin_reader` to get stdin and handle edits.

        :param prompt: The string or function that returns a string to be used
            as the prompt for the input line, defaults to ``self.prompt``
        :type prompt: str, function, optional

        :param pos_line: Line, or y, to draw the input on, defaults to current
            cursor position
        :type pos_line: int, optional

        :param pos_column: column, or x, to draw the input on, defaults to
            column 1
        :type pos_column: int, optional

        :param max_width: The max width of the input line, defaults to fill
            terminal
        :type max_width: int, optional
        """
        reader.start()

        self.state.pos_line = pos_line
        self.state.pos_column = pos_column or 1
        self.state.max_width = max_width

        def goto_index(i=None, relative=False, reference='l'):
            l = self.state.index
            r = self.state.r_index

            if i is not None:
                if reference == 'r':
                    if relative:
                        r += i
                    else:
                        r = i
                    l = len(self.state.stack) - 1 - r
                else:
                    if relative:
                        l += i
                    else:
                        l = i

            self.state.index   = clamp(l, 0, len(self.state.stack))
            self.state.r_index = len(self.state.stack) - self.state.index


        def handle_event(event):
            if event.name == '[':
                self.state.h_scroll += 1
                return
            if event.name == ']':
                self.state.h_scroll -= 1
                return

            if event.special:
                key = event.name
                n = len(self.state.stack)
                if key == 'left':
                    if self.state.r_index < n:
                        goto_index(-1, True)


                elif key == 'right':
                    if self.state.r_index > 0:
                        goto_index(1, True)
                        if (self.state.h_overflow
                            and (self.state.r_index < self.state.h_scroll)):
                            self.state.h_scroll -= 1

                elif key == 'up':
                    if self.state.history_index >= -len(self.state.history) + 1:
                        self.state.history_index -= 1

                    self.state.stack = [*self.state.history[self.state.history_index]]
                    goto_index(0, reference='r')
                    self.draw()

                elif key == 'down':
                    if self.state.history_index < -1:
                        self.state.history_index += 1

                    self.state.stack = [*self.state.history[self.state.history_index]]
                    goto_index(0, reference='r')
                    self.draw()

                elif key == 'backspace':
                    if self.state.stack and self.state.index != 0:
                        self.state.stack.pop(self.state.index - 1)
                        goto_index(-1, True)
                    if self.state.h_overflow and self.state.h_scroll:
                        self.state.h_scroll -= 1


                elif key == 'delete':
                    if self.state.stack and self.state.r_index != 0:
                        self.state.stack.pop(self.state.index)
                        goto_index()

                        if self.state.h_scroll > 0:
                            self.state.h_scroll -= 1

            elif event.name == '\n':
                sys.stdout.write(f'\n{esc.color.normal}')

                output = ''.join(self.state.stack)
                self.state.history[-1] = [*self.state.stack]

                self.state.stack.clear()
                goto_index(0)
                self.state.h_scroll = 0

                if output:
                    self.state.history.append('')
                self.state.history_index = -1
                return output
            else:
                self.state.history_index = -1
                if self.state.r_index:
                    self.state.stack.insert(-self.state.r_index, event.name)
                    if self.state.h_overflow:
                        if self.state.r_index == self.state.h_scroll:
                            self.state.h_scroll -= 1
                        else:
                            self.state.h_scroll += 1

                else:
                    self.state.stack.append(event.name)
                goto_index(1, True)

                self.state.history[-1] = self.state.stack

        try:
            while True:
                self.draw(prompt=prompt)

                if self.state.h_overflow:
                    column = self.state.max_column + (self.state.h_scroll - self.state.r_index)
                else:
                    column = self.state.pos_column + self.state.prompt_width + self.state.index
                sys.stdout.write(esc.cursor.goto(column=column))
                sys.stdout.flush()

                events = reader.pull_and_parse()
                if not events:
                    continue

                for event in events:
                    res = handle_event(event)

                    if self.state.r_index > self.state.available_input_space + self.state.h_scroll:
                        self.state.h_scroll += 1

                    if res:
                        return res

        except KeyboardInterrupt:
            print()
            raise KeyboardInterrupt()

        finally:
            reader.stop(wait=True)


    def _render_line_input_(self, *, prompt=None):
        unstyled_input = ''.join(self.state.stack)
        tokens = self.lexer.tokenize(unstyled_input)

        if prompt is None:
            prompt = self.prompt

        if callable(prompt):
            prompt_output = prompt()
        else:
            prompt_output = str(prompt)

        # prompt_output = self.prompt() if self.prompt else ''
        self.state.prompt_width = len(prompt_output)

        columns = esc.terminal.get_columns()
        if not self.state.max_width:
            self.state.max_width = columns
        if self.state.pos_column + self.state.max_width > columns:
            self.state.max_width = columns - self.state.pos_column

        self.state.max_column = self.state.pos_column + self.state.max_width

        self.state.available_input_space = self.state.max_width - self.state.prompt_width

        if self.state.prompt_width + len(unstyled_input) > self.state.max_width:
            if not self.state.h_overflow: self.state.h_overflow = True

            line = ''
            size = len(prompt_output) + 1
            self.state.available_input_space -= 1

            if len(tokens) == 1:
                line = tokens[0].styled_substring(
                    start=-(self.state.max_width - size + self.state.h_scroll),
                    end=-self.state.h_scroll or None
                )
            else:
                # my brain hurts now
                index_start = len(self.state.stack) - self.state.available_input_space - self.state.h_scroll
                index_end   = index_start + self.state.available_input_space

                i = 0
                for token in tokens:
                    token_output = ''
                    token_len = len(token.content)
                    span = i + token_len

                    if span <= index_start:
                        i += token_len
                        continue

                    if i >= index_end:
                        break

                    start = None
                    end   = None

                    # Start index is between the indicies that this token spans
                    if i < index_start < span < index_end:
                        start = -(span - index_start)

                    elif index_start < i < index_end < span:
                        start = 0
                        end = index_end - i

                    elif i < index_start < index_end < span:
                        start = -(span - index_start - 1)
                        # start = index_start - i
                        # end   = start + (span - index_end) - 1

                    line += token.styled_substring(start=start, end=end)
                    i += token_len

            return (
                f'{prompt_output}'
                f'{esc.style.invert}<{esc.style.invert_off}'
                f'{line}'
            )
        else:
            if self.state.h_overflow: self.state.h_overflow = False
            return f'{prompt_output}{self.lexer.render(tokens)}'


    def _render_line_hinting_(self, ):
        raise NotImplementedError()

    def draw(self, *, prompt=None):
        line_input = self._render_line_input_(prompt=prompt)
        # line_hinting = self._render_line_hinting_()

        if self.state.pos_column:
            goto_start = esc.cursor.goto(column=self.state.pos_column)
        else:
            goto_start = '\r'

        sys.stdout.write(
            goto_start
            # + esc.cursor.up(1)
            # + esc.clear_line
            # + f'{self.state.index}/{len(self.state.stack)} r:{self.state.r_index} scroll:{self.state.h_scroll} space:{self.state.available_input_space}'
            # + goto_start
            # + esc.cursor.down(1)
            + esc.clear_line
            + goto_start
            + line_input
            + esc.color.normal
        )
        sys.stdout.flush()
