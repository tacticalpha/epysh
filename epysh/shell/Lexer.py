
import re

import epysh.esc as esc

from epysh.classes import Namespace

class LexerSyntaxError(BaseException):
    pass

class Token(object):
    __slots__ = ['content', 'style_start', 'style_end', 'definition', 'start_index']
    def __init__(self, *, content, style_start, style_end, definition, start_index):
        self.content     = content
        self.style_start = style_start
        self.style_end   = style_end
        self.definition  = definition
        self.start_index = start_index

    @property
    def type(self):
        return self.definition.name

    @property
    def styled_content(self):
        return f'{self.style_start}{self.content}{self.style_end}'

    def styled_substring(self, *, start=None, end=None):
        return f'{self.style_start}{self.content[start:end]}{self.style_end}'

    def __str__(self):
        return (
            f'<epysh.shell.Lexer.Token definition.name={repr(self.type)} '
            f'start_index={self.start_index} '
            f'{"unimportant " if self.definition.unimportant else ""}'
            f'content={repr(self.content)}>'
        )
    def __repr__(self):
        return str(self)


class TokenDefinition(object):
    __slots__ = [
        'name', '_pattern_', '_type_', 'condition', 'style',
        'style_end', 'unimportant'
    ]
    def __init__(
        self, name, pattern, *, condition=None, style=None, style_end=None,
        unimportant=False
    ):
        self.name        = name
        self.pattern     = pattern
        self.condition   = condition
        self.style       = style
        self.style_end   = style_end
        self.unimportant = unimportant

    @property
    def type(self):
        return self._type_

    @property
    def pattern(self):
        return self._pattern_

    @pattern.setter
    def pattern(self, v):
        self._pattern_ = v
        self._type_    = type(v)


    def _match_(self, *, ctx):
        target = ctx.unconsumed

        if callable(self.pattern):
            is_match, match_length = self.pattern(target=target, ctx=ctx)
            res_group_type = type(res_group)
            if res_group_type == str:
                res_group = len(str)
            elif res_group_type == int:
                pass
            else:
                raise ValueError(
                    'TokenDefinition pattern should return (is_match: bool, '
                    'match_length: Union[int, str]). type(match_length) of'
                    f'"{res_group_type}" is not valid.'
                )
            return bool(res), res_sub

        if self.type == re.Pattern:
            match = self.pattern.match(target)
            return bool(match), len(match.group()) if match else None

        if self.type == str:
            if target.startswith(self.pattern):
                return True, len(self.pattern)
            else:
                return False, None

        else:
            raise RuntimeError(
                f'Unacceptable TokenDefinition pattern type "{self.type}". Type'
                'must be callable or in (re.Pattern, str)'
            )

    def match(self, *, ctx):
        is_match, match_length = self._match_(ctx=ctx)
        if self.condition and not self.condition(ctx=ctx):
            return False, None

        return is_match, match_length

    def get_style(self, *, ctx):
        style     = None
        style_end = None
        if callable(self.style):
            style = self.style(ctx=ctx)
        elif self.style:
            style = str(self.style)

        if callable(self.style_end):
            style_end = self.style_end(ctx=ctx)
        elif self.style_end:
            style_end = str(self.style_end)

        return style, style_end


class Lexer(object):
    def __init__(self, *, default_style=esc.color.normal):
        self.default_style = default_style

        self.tokens = {}
        self.state = Namespace(

        )

    def add(self, name, pattern, *, regex=True, state=None, **token_kwargs):
        if state not in self.tokens:
            self.tokens[state] = []

        if type(pattern) == str and regex:
            pattern = re.compile(pattern)

        self.tokens[state].append(TokenDefinition(name, pattern, **token_kwargs))

    def tokenize(self, syntax, *, get_styles=True):
        def find_last(*, filter=None):
            if not ctx.tokens:
                return None

            if callable(filter):
                i = len(ctx.tokens)
                while (i := i - 1) >= 0:
                    if filter(ctx.tokens[i]):
                        return ctx.tokens[0]
                return None

            return ctx.tokens[-1]

        ctx = Namespace(
            unconsumed = syntax,
            tokens     = [],
            all_tokens = [],
            index      = 0,
            find_last  = find_last
        )


        def consume(n):
            sub = ctx.unconsumed[:n]
            ctx.unconsumed = ctx.unconsumed[n:]
            ctx.index += n
            return sub

        while ctx.unconsumed:
            current_index = ctx.index

            for token_def in self.tokens[None]:
                is_match, match_length = token_def.match(ctx=ctx)
                if is_match:
                    content = consume(match_length)
                    if get_styles:
                        style_start, style_end = token_def.get_style(ctx=ctx)

                        style_start = style_start or self.default_style or ""
                        style_end = style_end or ""
                    else:
                        style_start, style_end = None

                    token = Token(
                        content        = content,
                        style_start    = style_start,
                        style_end      = style_end,
                        definition     = token_def,
                        start_index    = current_index
                    )

                    if not token_def.unimportant:
                        ctx.tokens.append(token)

                    ctx.all_tokens.append(token)

        return ctx.all_tokens

    def render(self, tokens, *, no_style=False):
        out = ''
        for token in tokens:
            if token.style_start is None and not no_style:
                raise RuntimeError(
                    'render requires tokenize to have been run with get_styles='
                    'True (default kwarg). to ignore this run render with '
                    'no_style=True'
                )
            out += token.styled_content

        return out
