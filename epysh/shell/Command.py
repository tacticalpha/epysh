
class Command(object):
    def __init__(
        self,
        function
        *,
        name=None,
    ):
        self.function = function

        if name:
            self.name = name
        else:
            self.name = function.name
