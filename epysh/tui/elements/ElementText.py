
from .Element import Element

class ElementText(Element):
    def init(self, *, text=''):
        self.value = text

    def render(self):
        return self.value
