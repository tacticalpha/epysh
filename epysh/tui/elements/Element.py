
class Span(object):
    __slots__ = ['start', 'end']
    def __init__(self, start=0, end=0):
        self.start = start
        self.end = end

    def contains(self, index):
        return self.start <= index <= self.end


class Element(object):
    def __init__(self, *, ctx, container, **element_kwargs):
        self.ctx = ctx
        self.container = container

        self.span = Span()
        self.listeners = {}
        self.value = ''

        if hasattr(self, 'init'):
            self.init(**element_kwargs)

    def dispatch(self, event, *args, **kwargs):
        for listener in listeners['event']:
            try:
                listener(*args, **kwargs)
            except Exception as e:
                self.ctx.app.error(context='Element listener', error=e)

    def add_event_listener(self, event, function):
        if event not in self.listeners:
            self.listeners[event] = []

        self.listeners[event].append(function)
