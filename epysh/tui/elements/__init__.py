
from .Element import Element

from .ElementText import ElementText

element_dict = {
    'element': Element,
    'text': ElementText
}
