
import sys

import epysh.esc as esc

from epysh.classes import Namespace
from epysh.esc.cursor import goto, left

from .Bounds import Bounds
from .Styles import Styles

class LayoutChild(object):
    default_style = Styles({
        'border': 'normal',
        'border:focus': 'double',

        'border_color<color>': 'bright_black',
        'border_color:focus': 'bright_blue',

        'label_color<color>': 'normal',
    })
    # default_style = Namespace(
    #     border       = 'normal',
    #     border_color = 'bright_black',
    #
    #     label_color = 'normal',
    # )
    # default_style_focus = Namespace(
    #     border       = 'double',
    #     border_color = 'bright_blue',
    # )

    def __init__(self, *, ctx, grow=1, label=None, styles=None):
        self.ctx  = ctx
        self.grow = grow

        self.label = label

        self.bounds = Bounds()

        self.styles = Styles.apply(None, LayoutChild.default_style, styles)

    def draw_border(self, pos=None, dir=None):
        # TODO: Clean this up. I dont like this solution but I
        #       just want to get this working

        # box_chars = (
        #     '┌┬┐',
        #     '├ ┤',
        #     '└┴┘'
        # )
        # box_char_sets = {
        #     'row': {
        #         'start':
        #     }
        # }

        corner_sets = {
            None           : ('┌┐'
                              '└┘'),

            'start_row'    : ('┌┬'
                              '└┴'),

            'end_row'      : ('┬┐'
                              '┴┘'),

            'middle_column': ('├┤'
                              '├┤'),

            'middle_row'   : ('┬┬'
                              '┴┴'),

            'start_column' : ('┌┐'
                              '├┤'),

            'end_column'   : ('├┤'
                              '└┘'),
        }

        self.styles.set_state('focus', self.ctx.app.state.focus == self)

        color_border = self.styles.get('border_color')
        color_label  = self.styles.get('label_color')

        if pos is None or dir is None:
            corners = corner_sets[None]
        else:
            corners = corner_sets[f'{pos}_{dir}']

        label = str(self.label)

        home = goto(self.bounds.top_left)
        tb = '─' * (self.bounds.width - 2)
        l = ''
        r = ''
        for i in range(1, self.bounds.height - 1):
            # r += goto(column=self.bounds.x1 - 1, line=self.bounds.y1 + i) + '│'
            l += goto(column=self.bounds.x0, line=self.bounds.y0 + i) + '│'
            r += goto(column=self.bounds.x1, line=self.bounds.y0 + i) + '│'

        self.ctx.write(
            esc.color.reset
            + color_border
            + home
            + corners[0] + (
                tb[0] + color_label + label +color_border + tb[len(label) + 1:] if self.label else tb
            ) + corners[1]
            # + corners[0] + tb + corners[1] + ( goto(column=self.bounds.x0 + 2, line=self.bounds.y0) + f'{self.label}' if self.label else '')
            + r
            + home
            + l
            # + goto(column=self.bounds.x1, line=self.bounds.y0)
            # + home + ('\n' * (self.bounds.height - 2)) + '\n'
            # + '\n'
            + goto(self.bounds.bottom_left)
            + corners[2] + tb + corners[3]
            # + home + f'{self.bounds.x0, self.bounds.y0 if self.bounds.y0 == 0 else (self.bounds.y0 - 0)}'
        )
