
import math
import sys

import epysh.esc as esc

from epysh.classes import Coords

def round_coord(v):
    try:
        return math.floor(v)
    except TypeError:
        return v

class Size(object):
    __slots__ = ['columns', 'lines']
    def __init__(self, *, columns, lines):
        self.columns = int(columns)
        self.lines   = int(lines)

class Bounds(object):
    __slots__ = ['_x0_', '_y0_', '_x1_', '_y1_']
    def __init__(self, x0=1, y0=1, x1=None, y1=None):
        self.set(x0, y0, x1, y1)

    def set(self, x0=1, y0=1, x1=None, y1=None):
        if x1 == x0 or y1 == y0:
            raise RuntimeError('Bounds object can\'t have a width or height of 0')

        if x1 is not None and x1 < x0:
            x0, x1 = x1, x0
        if y1 is not None and y1 < y0:
            y0, y1 = y1, y0

        self.x0 = x0
        self.y0 = y0

        self.x1 = x1
        self.y1 = y1

    @property
    def x0(self):
        return self._x0_
    @x0.setter
    def x0(self, v):
        if type(v) == int and v < 0:
            v = esc.terminal.get_columns() + v
        self._x0_ = round_coord(v)

    @property
    def y0(self):
        return self._y0_
    @y0.setter
    def y0(self, v):
        if type(v) == int and v < 0:
            v = esc.terminal.get_lines() + v
        self._y0_ = round_coord(v)


    @property
    def x1(self):
        if self._x1_:
            return self._x1_
        elif self._x1_ is None:
            return esc.terminal.get_columns()
    @x1.setter
    def x1(self, v):
        if type(v) == int and v < 0:
            v = esc.terminal.get_columns() + v
        self._x1_ = round_coord(v)


    @property
    def y1(self):
        if self._y1_:
            return self._y1_
        elif self._y1_ is None:
            return esc.terminal.get_lines()
    @y1.setter
    def y1(self, v):
        if type(v) == int and v < 0:
            v = esc.terminal.get_lines() + v
        self._y1_ = round_coord(v)

    @property
    def a(self):
        return Coords(column=self.x0, line=self.y0)

    @property
    def b(self):
        return Coords(column=self.x1, line=self.y1)

    @property
    def top_left(self):
        return Coords(column=self.x0, line=self.y0)

    @property
    def top_right(self):
        return Coords(column=self.x1, line=self.y0)

    @property
    def bottom_left(self):
        return Coords(column=self.x0, line=self.y1)

    @property
    def bottom_right(self):
        return Coords(column=self.x1, line=self.y1)

    @property
    def size(self):
        return Size(columns=self.width, lines=self.height)

    @property
    def width(self):
        return (self.x1 - self.x0) + 1

    @property
    def height(self):
        return (self.y1 - self.y0) + 1

    def __iter__(self):
        return iter((self.x0, self.y0, self.x1, self.y1))

    def __eq__(self, other):
        if hasattr(other, '__iter__'):
            return iter(self) == iter(other)
            # return (not (
            #     (self.x0 != iter[0])
            #     or (self.y0 != iter[1])
            #     or (self.x1 != iter[2])
            #     or (self.y1 != iter[3])
            # ))
        elif type(other) == Bounds:
            return iter(self) == iter(other)
            # return (not (
            #     (self.x0 != other.x0)
            #     or (self.y0 != other.y0)
            #     or (self.x1 != other.x1)
            #     or (self.y1 != other.y1)
            # ))
        raise RuntimeError('can\'t compare with object of type ' + str(type(other)))

    def draw(self):
        color = esc.color.get(esc.color.color_names[id(self) % 7], bg=True)
        sys.stdout.write(
            color
            + esc.cursor.goto(column=self.x0, line=self.y0)
            # + f'({self.x0}, {self.y0})'
            + '0'
            + esc.cursor.goto(column=self.x1, line=self.y1)
            # + f'({self.x1}, {self.y1})'
            + '1'
            + esc.color.reset
        )

    def local_to_global(self, coords=None, *, column=None, line=None):
        column, line = Coords.consolidate(coords, column, line)
        return Coords(
            # column = (self.x1 if column < 0 else self.x0) + column,
            column = self.x0 + column,
            # line   = (self.x1 if line < 0 else self.x0) + line
            line   = self.y0 + line
        )

    def contains(self, coords=None, *, column=None, line=None):
        column, line = Coords.consolidate(coords, column, line)
        return (self.x0 <= column <= self.x1) and (self.y0 <= line <= self.y1)
