
import math
import time
import sys

import epysh.esc as esc

from .LayoutChild import LayoutChild
from .OutputBox import OutputBox
from .Bounds import Bounds

class Container(LayoutChild):
    def __init__(self, *, ctx, **layout_kwargs):
        super().__init__(ctx=ctx, **layout_kwargs)

        self.children = []

        self.direction    = 'column'

    def reflow(self):
        is_row = self.direction == 'row'
        if is_row:
            each_width = None
            each_height = self.bounds.height
        else:
            each_width = self.bounds.width
            each_height = None

        n = len(self.children)
        total_grow = sum([ child.grow for child in self.children ])

        start = self.bounds.x0 if is_row else self.bounds.y0
        space = self.bounds.width if is_row else self.bounds.height

        pos = start

        for i, child in enumerate(self.children):
            size = math.floor(space * (child.grow / total_grow))

            if is_row:
                desired_bounds = (
                    pos + (0 if i == 0 else 0),
                    self.bounds.y0,
                    self.bounds.x1 if i == n - 1 else pos + size,
                    self.bounds.y1
                )
            else:
                desired_bounds = (
                    self.bounds.x0,
                    pos + (0 if i == 0 else 0),
                    self.bounds.x1,
                    self.bounds.y1 if i == n - 1 else pos + size
                )

            pos = pos + size + 1

            # if desired_bounds == child.bounds:
            #     continue


            child.bounds.set(*desired_bounds)
            if hasattr(child, 'reflow'):
                # TODO: See LayoutChild.py
                # if child.style.border:
                #     last_has_border = i != 0 and self.children[i-1].style.border
                #     next_has_border = i != (n - 1) and self.children[i+1].style.border
                #     if last_has_border and next_has_border:
                #         border_pos = 'middle'
                #     elif last_has_border:
                #         border_pos = 'end'
                #     elif next_has_border:
                #         border_pos = 'start'
                #     else:
                #         border_pos = None
                #
                #     if last_has_border:
                #         # child.bounds._y0_ -= 1
                #         pass
                #
                #     if next_has_border:
                #         child.bounds._y1_ += 1
                #         pass

                    # time.sleep(1)
                    # child.draw_border(None)

                # sys.stdout.write(esc.cursor.home + f'child {i + 1}/{n} ({child.bounds.x0}, {child.bounds.y0}) size {size} pos {pos}\n\r{desired_bounds[0:2]}')
                # i += 1
                # sys.stdout.flush()
                # time.sleep(1)
                if not hasattr(child, 'children') or not child.children:
                    child.draw_border(None, self.direction)
                    # child.bounds.draw()
                    # self.ctx.write(esc.cursor.goto(column=child.bounds.x0 + 2, line=child.bounds.y0) + f'child {i} {"not" if i != 0 else ""} first')
                child.reflow()
                # child.draw_border(border_pos, self.direction)


    def add_container(self, *, index=None, **container_kwargs):
        container = Container(ctx=self.ctx, **container_kwargs)
        if index:
            self.children.insert(index, container)
        else:
            self.children.append(container)

        return container

    def add_output_box(self, *, index=None, **output_box_kwargs):
        output_box = OutputBox(ctx=self.ctx, **output_box_kwargs)
        if index:
            self.children.insert(index, output_box)
        else:
            self.children.append(output_box)

        return output_box
