
import sys
import re

from epysh.classes import Coords
from epysh.esc import cursor

from .Bounds import Bounds

# stolen from https://github.com/chalk/ansi-regex
# escape_code_string = (
#     '[\033\u001B\u009B][\[\\\]\(\)#;\?]*(?:(?:(?:(?:;[-a-zA-Z\d\/#&.:=?%@~_]+)*|[a-zA-Z\d]+(?:;[-a-zA-Z\d\/#&.:=?%@~_]*)*)?\u0007)'
#     '|(?:(?:\d{1,4}(?:;\d{0,4})*)?[\dA-PR-TZcf-nq-uy=><~]))'
# )

escape_code_string = re.compile(r'''
      # ESC
    (
        \033 # 7-bit C1 Fe (except CSI)
        [@-Z\\-_]
    |     # or [ for CSI, followed by a control sequence
        \[
        [0-?]*  # Parameter bytes
        [ -/]*  # Intermediate bytes
        [@-~]   # Final byte
    )|([\r\n])|(.+)
''', re.VERBOSE)
# )|(.)''', re.VERBOSE)

escape_code_regex = re.compile(escape_code_string)

# print(repr(re.sub(escape_code_string, '!', 'hello\033[Hhiiii')))

class BoundWriter(object):
    def __init__(self):
        self.bounds = Bounds()
        self.local_cursor_pos = Coords(column=0, line=0)

    # def goto(self, coords=None, * column=None, line=None):
    #     column, line = Coords.consolidate(coords, column, line)

    def write(
        self, content, *, coords=None, local_column=None, local_line=None,
        flush=True, strip_after_break=True, hyphenate_line_break=False, write_func=sys.stdout.write
    ):
        local_column, local_line = Coords.consolidate(coords, local_column, local_line)
        if local_column is not None:
            self.local_cursor_pos.column = local_column
        if local_line is not None:
            self.local_cursor_pos.line = local_line

        content = str(content)
        global_coords = self.bounds.local_to_global(self.local_cursor_pos)

        available_columns = self.bounds.width - self.local_cursor_pos.column
        available_lines   = self.bounds.height - self.local_cursor_pos.column

        out = ''
        size = 0
        # matches = re.findall(escape_code_string, '', content)
        matches = escape_code_regex.findall(content)
        for escape_code, control, chunk in matches:
            if chunk and (self.local_cursor_pos.column + len(chunk) > available_columns):
                start_line = global_coords.line

                lines = ''
                i = 0
                while chunk:
                    lines += cursor.goto(column=global_coords.column, line=start_line + i)

                    cur_line = chunk[0:available_columns]
                    chunk    = chunk[available_columns:]
                    if hyphenate_line_break:
                        cur_line = cur_line.rstrip() + '-'

                    self.local_cursor_pos.column = global_coords.column + len(cur_line)

                    lines += cur_line
                    i += 1

                self.local_cursor_pos.line += i - 1
                out += lines

                # out += '\n' + cursor.goto(column=global_coords.column)
                size = 0

            elif escape_code:
                out += '\033' + escape_code
            elif control:
                if control == '\r':
                    self.local_cursor_pos.column = 1
                elif control == '\n':
                    self.local_cursor_pos.line += 1

                out += cursor.goto(self.bounds.local_to_global(self.local_cursor_pos))
            else:
                out += chunk
                self.local_cursor_pos.column += len(chunk)

        # home = cursor.goto(line=self.bounds.x0, column=self.bounds.y0)
        home = cursor.goto(global_coords)
        out.replace('\r', cursor.goto(column=self.bounds.x0))

        write_func(
            cursor.goto(global_coords)
            # + repr(cursor.goto(global_coords))
            + out
            # repr(out)
        )
