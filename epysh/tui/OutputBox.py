
from .elements import element_dict

from .BoundWriter import BoundWriter
from .LayoutChild import LayoutChild

class OutputBox(LayoutChild):
    def __init__(self, *, ctx, **layout_kwargs):
        LayoutChild.__init__(self, **layout_kwargs, ctx=ctx)

        self.writer = BoundWriter()
        self.elements = []

    def reflow(self):
        self.writer.bounds.set(self.bounds.x0, self.bounds.y0, self.bounds.x1 - 1, self.bounds.y1 - 1)
        self.writer.local_cursor_pos.set(1, 1)

        content = ''
        for element in self.elements:
            content += element.render()

        self.writer.write(content, write_func=self.ctx.write)

    def add_element(self, name, **element_kwargs):
        name = name.lower()
        if name not in element_dict:
            raise KeyError(f'unknown element: "{name}"')

        element = element_dict[name](
            ctx=self.ctx,
            container=self,
            **element_kwargs
        )
        self.elements.append(element)
