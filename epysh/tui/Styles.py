
from collections import OrderedDict
import re

import epysh.esc as esc

selector_regexp = re.compile(
    '(?P<name>\w+)'
    '(?P<type><\w+>)?'
    '(?P<state>(?::\w+)+)?'
)


class Style(object):
    __slots__ = ['states', 'type']
    casters = {
        'int': int,
        'float': float,
        'color': esc.color.get,
    }
    def __init__(self):
        # self.name   = str(name).lower()
        self.states = OrderedDict({None: None})
        self.type   = None

    def __repr__(self):
        return f'(<{self.type}>{repr(self.states[None])}{"".join([ f" {state} -> {repr(self.states[state])}" for state in self.states if state is not None ])})'

    def __iter__(self):
        return iter(self.states)

    def get(self, states=None):
        if not states:
            states = None
        elif states and not hasattr(states, '__iter__'):
            states = [states]

        if states:
            states = [ str(state) for state in states if state ]

        def get_topmost_state():
            nonlocal self, states
            if states and len(states) == 1:
                if states[0] in self.states:
                    return states[0]
            elif states:
                top_index = -1
                keys    = list(self.states.keys())
                for state in states:
                    for i, key in enumerate(keys):
                        if state == key and i > top_index:
                            top_index = i

                if top_index != -1:
                    return keys[top_index]

            return None

        val = self.states[get_topmost_state()]

        if self.type and self.type in Style.casters:
            return Style.casters[self.type](val)

        return val

    def set(self, value, *, state=None, type=None):
        state = str(state) if state else None
        if type is not None:
            self.type = str(type).lower()

        self.states[state] = value

    @staticmethod
    def apply(target, *others):
        if target is None:
            target = Style()

        for other in others:
            for state in other.states:
                target.states[state] = other.states[state]

            if other.type:
                target.type = other.type

        return target



class Styles(object):
    def __init__(self, styles_dict=None):
        self.styles = {}
        self.states = []

        if styles_dict:
            for k in styles_dict:
                self[k] = styles_dict[k]

    @staticmethod
    def __parse_selector__(k):
        if not k:
            return None

        k = str(k).lower()
        match = selector_regexp.match(k)
        return match

    def get(self, k):
        return self[k]

    def __getitem__(self, k):
        sel = Styles.__parse_selector__(k)
        if not sel:
            return None

        if sel.group('name') not in self.styles:
            return None

        if sel.group('state'):
            states = [ state for state in sel.group('state').split(':') if state ]

        return self.styles[sel.group('name')].get(states=[sel.group('state')] if sel.group('state') else self.states)

    def set(self, k, v):
        self[k] = v

    def __setitem__(self, k, v):
        sel = Styles.__parse_selector__(k)
        if not sel:
            return None

        name = sel.group('name')
        if name not in self.styles:
            self.styles[name] = Style()

        state = sel.group('state').split(':')[-1] if sel.group('state') else None
        type  = sel.group('type').strip('<>')     if sel.group('type')  else None

        self.styles[name].set(
            v,
            state=state,
            type=type
        )

    def __iter__(self):
        return iter(self.styles.keys())

    def set_state(self, state, setting=True):
        if setting and state not in self.states:
            self.states.append(state)
        elif not setting and state in self.states:
            self.states.remove(state)

    @staticmethod
    def apply(target, *others):
        if target is None:
            target = Styles()

        for other in others:
            if not other or type(other) != Styles:
                continue

            for name in other:
                style = other.styles[name]
                if name in target.styles:
                    Style.apply(target.styles[name], style)
                else:
                    target.styles[name] = Style.apply(None, style)

        return target
