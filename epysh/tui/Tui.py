
import time
import sys

import epysh.stdin_reader as reader
import epysh.esc as esc

from epysh.classes import Namespace

# from .BoundedWritter import BoundWritter
from .Container import Container
from .Bounds import Bounds

class Tui(object):
    # def __init__(self, pos_column=0, pos_line=0, width=None, height=None):
    #     super().__init__(Bounds(
    #         pos_column,
    #         pos_line,
    #         pos_column + width if width else None,
    #         pos_line + height if height else None
    #     ))
    def __init__(self):
        self.ctx = Namespace(
            app   = self,
            write = self._write_,
            flush = self._flush_
        )

        self.layout = Container(ctx=self.ctx)
        self.state = Namespace(
            lastSize  = None,
            lastError = None,
            focus     = None,
            dirty     = False,
            buffer    = ''
        )

        # self.padding = (0, 0, 0, 0)

    def dirty(self):
        if not self.state.dirty:
            self.state.dirty = True

    def _write_(self, s):
        self.state.buffer += s

    def _flush_(self):
        buffer = (
            (esc.terminal.clear_screen if self.state.dirty else '')
            + self.state.buffer
        )

        self.state.buffer = ''
        self.state.dirty  = False

        sys.stdout.buffer.write(buffer.encode())
        sys.stdout.flush()

    def error(self, *, error, context=None):
        self.state.lastError = {'error': error, 'context': context}
        self.dirty()

    def __draw_too_small_error__(self):
        self.state.buffer = ''
        self._write_(
            esc.cursor.home
            + esc.style.reset
            + esc.style.invert
            + 'Terminal is too small :('
        )
        self._flush_()


    def __draw_profiler__(self, size, reflow_seconds):
        reflow_ms  = reflow_seconds * 1000
        reflow_fps = 1000/reflow_ms
        profile_str = f'reflow timing: {reflow_ms:0>6.2f}ms {reflow_fps:>3.0f}rps '
        self._write_(
            esc.cursor.goto(column=0, line=0)
            + esc.style.reset
            + esc.style.invert
            + ' ' * (size.columns - len(profile_str))
            + profile_str
        )

    # def __draw_error__(self):

    def draw(self, *, profile_reflow=False):
        size = esc.terminal.get_size()

        if profile_reflow:
            self.layout.bounds.y0 = 2
            start_time = time.time()

        if self.state.lastError:
            self.layout.bounds.y1 = -1

        if size != self.state.lastSize:
            self.dirty()

        try:
            self.layout.reflow()
        except Exception as e:
            name = type(e).__name__
            msg = e.args[0].lower() if e.args else None
            if msg == 'bounds object can\'t have a width or height of 0':
                self.__draw_too_small_error__()
                self.state.lastSize = size
                return


        if profile_reflow:
            self.__draw_profiler__(size, time.time() - start_time)

        if self.state.lastError:
            err = self.state.lastError
            msg = (
                'error :( │ '
                + (f'{err["context"]}: ' if err['context'] else '')
                + err['error']
            )
            self._write_(
                esc.cursor.goto(column=0, line=self.layout.bounds.y1 + 1)
                + esc.color.get(202)
                + esc.style.invert
                # + esc.terminal.clear_line
                # + 'errrorr :('
                + msg
                + ' ' * (size.columns - len(msg))
            )

        sys.stdout.write(esc.cursor.invisible)
        self._flush_()
        self.state.lastSize = size

    def loop(self):
        try:
            sys.stdout.write(esc.terminal.use_alternate_buffer + esc.terminal.clear_screen + esc.cursor.home + esc.cursor.invisible)
            reader.start()
            while True:
                resp = reader.pull_and_parse(timeout=0.5)
                ui.draw()
        except:
            pass
        finally:
            sys.stdout.write(esc.terminal.use_normal_buffer + esc.cursor.visible)
            reader.stop()
