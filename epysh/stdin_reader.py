
import threading
import termios
import select
import time
import tty
import sys
import os

import epysh.esc.parser

def _worker_func_(
        *, buffer, semaphore, stop_event,
        mode, loop_timeout, decode
    ):
    fd = sys.stdin.fileno()
    original_settings = termios.tcgetattr(fd)
    def setup():
        if mode == 'cbreak':
            tty.setcbreak(fd)
        elif mode == 'raw':
            tty.setraw(fd)
        else:
            raise ArgumentError(f'invalid read mode "{mode}"')

    def loop():
        if stop_event.is_set():
            return 1

        r, _, _ = select.select([ sys.stdin ], [], [], loop_timeout)
        if sys.stdin in r:
          data = os.read(sys.stdin.fileno(), 16)
        else:
          return

        buffer.append(data.decode(decode) if decode else data)
        semaphore.release()

    try:
        setup()
        while not loop(): pass
    except KeyboardInterrupt:
        return
    finally:
        termios.tcsetattr(fd, termios.TCSADRAIN, original_settings)
        sys.stdin.flush()
        release_mouse()

buffer             = []
read_lock          = threading.Lock()
semaphore_readable = threading.Semaphore(0)
event_stop         = threading.Event()

def _clear_semaphore_readable_():
    while semaphore_readable._value > 0:
        semaphore_readable.release()

thread = None
def start(*, restart=True, timeout=None, mode='cbreak', loop_timeout=0.05, decode='utf-8'):
    """Starts stdin reader thread

    :param restart: Automatically restart thread with new paramaters if it's
        running already, defaults to True
    :type restart: bool, optional

    :param mode: stdin read mode, `cbreak` or `raw`, defaults to `cbreak`
    :type decode: str, optional

    :param loop_timeout: Time to pause while waiting for stdin to become
        readable in worker thread, or forever if None. defaults to None
    :type loop_timeout: float, optional

    :param decode: output.decode() argument or None if stdin output shouldn't be decoded, defaults to `utf-8`
    :type decode: str, optional

    :return: `threading.Thread` object
    """
    global thread
    if (not thread) or (thread and restart):
        if thread:
            stop(wait=True, timeout=timeout or -1)

        event_stop.clear()
        _clear_semaphore_readable_()
        thread = threading.Thread(
            target=_worker_func_,
            kwargs={
                'buffer': buffer,
                'semaphore': semaphore_readable,
                'stop_event': event_stop,

                'mode': mode,
                'loop_timeout': loop_timeout,
                'decode': 'utf-8',
            },
        )

        thread.start()
    return thread

def is_alive():
    """
    :returns: Whether or not worker thread is currently running
    :rtype: bool
    """
    return thread is not None and thread.is_alive()

# def restart():
#     if is_alive():
#         stop(wait=True)
#
#     thread.start()

def stop(*, wait=False, timeout=None):
    """
    :param wait: Should the function block until the thread rejoins the main thread, defaults to False
    :type wait: bool, option
    """
    global thread

    event_stop.set()
    release_mouse()
    if wait and is_alive():
        thread.join(timeout=timeout)
    thread = None

def flush():
    _clear_semaphore_readable_()
    buffer.clear()

def pull(*, into=None, timeout=None, acquire_lock=True, join=False):
    """Pulls all available input from worker thread buffer

    :param into: List to insert buffer elements into. A new list is returned if
        this isn't set, defaults to None
    :type into: list, optional

    :param timeout: How long to wait for content, or forever if None, defaults
        to None
    :type timeout: int, optional

    :param aquire_lock: Should aquire read lock before trying to pull data from
        worker buffer. This should usually be True. defaults to `True`
    :type aquire_lock: bool, optional

    :param join: Should return ``''.join(into)``. Not really necessary but its
        just a little QOL feature :) defaults to `False`
    :type join: bool, optional

    :return: `into` if it was provided or a new list containing the pulled
        items or a string if `join` == `True`
    :rtype: list or string
    """
    if into is None:
        into = []

    try:
        if acquire_lock:
            if not read_lock.acquire(timeout=timeout or -1):
                return None

        while True:
            if not semaphore_readable.acquire(timeout=timeout):
                return
            if buffer:
                break

        if buffer:
            into.append(buffer.pop(0))

        if join:
            return ''.join(into)

        return into
    except KeyboardInterrupt:
        stop()
        raise KeyboardInterrupt()
    finally:
        if acquire_lock and read_lock.locked():
            read_lock.release()

def pull_and_parse(*, delay_on_unknown=0.05, timeout=None):
    """A helper function to call :func:`epysh.stdin_reader.pull` then pipe
    the output through :func:`epysh.esc.parser.parse`

    :param timeout: See :func:`epysh.stdin_reader.pull`
    :type timeout: bool, optional

    :return: list of :class:`epysh.esc.parser.parsed_input` objects
    :rtype: list
    """

    data = pull(timeout=timeout)
    if not data:
        return None

    res = epysh.esc.parser.parse(data)
    if not res:
        raise ValueError('there was buffer data but esc.parser.parse returned nothing')

    # if res.unknown:
    #     time.sleep(delay_on_unknown)
    #     pull(into=data, timeout=0.05)
    #     res = epysh.esc.parser.parse(data)

    return res

def capture_mouse():
    sys.stdout.write('\033[?1003h\033[?1015h\033[?1006h')

def release_mouse():
    sys.stdout.write('\033[?1000l')
