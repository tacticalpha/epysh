.. epysh.rst

Base Module
------------

Alias of :class:`epysh.shell.Shell`

.. autoclass:: epysh.Shell
  :members:
  :noindex:
