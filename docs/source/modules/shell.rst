.. shell.rst:

Module ``shell``
=====================

.. Variables
.. -----------------------

Interface
-----------------------

.. autoclass:: epysh.shell.Shell
  :members:
  :noindex:
