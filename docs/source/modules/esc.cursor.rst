.. esc.cursor.rst:

Module ``esc.cursor``
======================


Hint: terminal coordinates are one indexed, not zero indexed. They start at
(1, 1) instead of (0, 0) like you might be used to!

Constants
--------------------

* ``home`` - Ansi escape code to move the cursor to to 1, 1
* ``invisible`` - Turn the cursor invisible
* ``visible`` - Turns the cursor visible
* ``echo_cursor_pos`` - Make the terminal write the current cursor coords to
  stdin.
* ``save_pos_term`` - Tells the terminal to remember the current cursor position
* ``restore_pos_term`` - Returns the cursor to previously saved position


Interface
-----------------------

.. automodule:: epysh.esc.cursor
  :members:
