.. esc.color.rst:

Module ``esc.color``
====================

Constants
--------------------

* ``reset``
* ``normal``

These constants are all strings containing the ansi escape code to output their
respective color.

* ``black``
* ``red``
* ``green``
* ``yellow``
* ``blue``
* ``magenta``
* ``cyan``
* ``white``

All colors also have counterparts with the prefixes ``bright_``, ``bg_``, or
``bg_bright_``

Interface
-----------------------

.. automodule:: epysh.esc.color
  :members:
  :noindex:
