.. esc.parser.rst:

Module ``esc.parser``
=====================

Variables
-----------------------

* ``escape_chars`` see :func:`epysh.esc.parser.define_char`
* ``escape_sequences`` see :func:`epysh.esc.parser.define_sequence`
* ``mouse_events`` see :func:`epysh.esc.parser.define_mouse_event`

Interface
-----------------------

.. automodule:: epysh.esc.parser
  :members:
