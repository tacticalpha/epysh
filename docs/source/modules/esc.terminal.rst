.. esc.terminal.rst:

Module ``esc.terminal``
=======================

Constants
--------------------

* ``use_alternate_buffer``
* ``use_normal_buffer``

* ``clear_screen``
* ``clear_line``


Interface
-----------------------

.. automodule:: epysh.esc.terminal
  :members:
