.. esc.style.rst:

Module ``esc.style``
====================

Constants
--------------------

* ``reset``

* ``bold``
* ``dim``
* ``italic``
* ``underline``
* ``blinking``
* ``inverted``
* ``invisible``
* ``strikethrough``

All styles also off counterparts prefixed with ``un`` (e.g. ``unbold``) or suffixed
with ``_off`` (e.g. ``bold_off``)

Interface
-----------------------

.. automodule:: epysh.esc.style
  :members:
