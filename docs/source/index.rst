.. epysh documentation master file, created by
   sphinx-quickstart on Tue Jan 18 00:48:51 2022.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

..
  Welcome to epysh's documentation!
  =================================

| Interested in learning more about how terminal escape codes work?
| `<https://gist.github.com/fnky/458719343aabd01cfb17a3a4f7296797>`_
  is a good reference I use a lot. The author explains things in a very easy to
  understand way.

An important difference between this module and others like ``ncurses`` is that
most of its methods return strings for you to write to the terminal yourself
unless they say otherwise, instead of immediately when you call them. This gives
more flexibility in performance and a more predictable output in my experience.

API docs :)
---------------------

🚧 Docs are under construction 🚧

.. toctree::
   :maxdepth: 3

   epysh
   stdin_reader
   modules/shell
   modules/cli.choice
   modules/esc.color
   modules/esc.cursor
   modules/esc.parser
   modules/esc.style
   modules/esc.terminal


.. Indices and tables
.. ==================
..
.. * :ref:`genindex`
.. * :ref:`modindex`
.. * :ref:`search`
