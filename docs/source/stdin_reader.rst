.. stdin_reader:

Module ``stdin_reader``
=======================

Interface
---------

.. automodule:: epysh.stdin_reader
  :members:
