# epysh
_epysh_ is a simple module for helping the writing of advanced terminal interfaces. A few key features are

- `epysh.esc`, a set of modules that provide almost any ansi escape codes you need
- `epysh.Shell`, A rich replacement for python's `input` including syntax highlighting, history, and cursor movement.
<!-- - `epysh.tui`, Work in progress -->

### Installation
-------
```bash
python3 -m pip install git+https://gitlab.com/tacticalpha/epysh.git
```

### Docs
-------
https://tacticalpha.gitlab.io/epysh/
